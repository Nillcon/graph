let draw = new Draw();
let parser = new Parser();
let calculator = new Calculator();
let tokenizator = new Tokenizator();

let zoom = 1;

document.addEventListener("DOMContentLoaded", () => {
    let canvas = document.querySelector('#graph');

    canvas.width = document.documentElement.clientWidth - 38;
    canvas.height = document.documentElement.clientHeight - 30;

    draw.drawMarkUp(canvas, 1);

    document.querySelector('#calculate').addEventListener('click', () => drawGraph(canvas))
    document.querySelector('#plusZoom').addEventListener('click', () => {
        zoom += 0.1;
        document.querySelector('#minusZoom').disabled = (zoom <= 0)? true : false;

        drawGraph(canvas)
    });
    document.querySelector('#minusZoom').addEventListener('click', () => {
        if (zoom >= 0.2) zoom -= 0.1;

        document.querySelector('#minusZoom').disabled = (zoom <= 0)? true : false;

        drawGraph(canvas)
    })
});

function drawGraph(canvas) {
    draw.clear(canvas);
    draw.drawMarkUp(canvas, zoom);

    let func = document.querySelector('#func').value;
    let x1 = Number(document.querySelector('#x1').value);
    let x2 = Number(document.querySelector('#x2').value);
    let step = Number(document.querySelector('#step').value);

    try {
        tabulate(canvas, func, x1, x2, zoom, step);
    } catch(e) {
        console.log(e);
        alert(e);
    }
}


function tabulate(canvas, funcStr, i1, i2, zoom, step) {

    let tree = parser.parse(funcStr);
    // Get prev x,y
    let dx = i1;
    let dy = tree.calculate({x: i1});

    let derivativeTree = tree.derivative();

    console.log(derivativeTree)

    // Draw graph
    for (let x = i1; x <= i2; x += step) {
        let y = tree.calculate({x: x});

        let df = derivativeTree.calculate({x: x});

        if (df !== Infinity && df !== -Infinity) {
            draw.gotoxy(canvas, (x) * (zoom*50), (y) * (zoom*50), (dx) * (zoom*50), (dy) * (zoom*50));
        }        

        dx = x;
        dy = y;
    }

    let S = calculator.calcSquare(tree, i1, i2, step);

    draw.text(canvas, `S = ${S}`, 15, 25, 20);
    console.log(S);
}

