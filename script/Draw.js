class Draw {
    constructor() {}

    draw(canvas, x1, y1, x2, y2, color = "black") {
        let ctx = canvas.getContext("2d");

        ctx.fillStyle = color;
        ctx.fillRect(x1, y1, x2, y2);
    }

    clear(canvas) {
        let context = canvas.getContext('2d');

        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    drawMarkUp(canvas, zoom = 50) {
        let space = 35.4;

        let width = canvas.width;
        let height = canvas.height;

        this.drawVerticalLine(canvas);
        this.drawHorizontalLine(canvas);

        //draw markup
        let numb = 1;
        for (let i = (canvas.clientHeight/2) + 50; i < canvas.clientHeight; i += 50) {
            this.lineTo(canvas, 0, i, canvas.clientWidth, i, '#d9dce2');
            this.lineTo(canvas, 0, canvas.clientHeight - i, canvas.clientWidth, canvas.clientHeight - i, '#d9dce2');

            this.text(canvas, `${(numb/zoom).toFixed(3)}`,  (canvas.clientWidth/2)+12, canvas.clientHeight - (i-3));
            this.text(canvas, `-${(numb/zoom).toFixed(3)}`,  (canvas.clientWidth/2)+15, i + 3);

            this.lineTo(canvas, (canvas.clientWidth/2)+6, i, (canvas.clientWidth/2)-5, i);
            this.lineTo(canvas, (canvas.clientWidth/2)+6, canvas.clientHeight - i, (canvas.clientWidth/2)-5, canvas.clientHeight - i);

            numb++;
        }

        numb = 1
        for (let i = (canvas.clientWidth/2) + 50; i < canvas.clientWidth; i += 50) {
            this.draw(canvas, i, 0, 1, height,  '#d9dce2');
            this.draw(canvas, width - i, 0, 1, height,  '#d9dce2');

            this.text(canvas, `${(numb/zoom).toFixed(3)}`, i-3, (canvas.clientHeight/2)+14);
            this.text(canvas, `-${(numb/zoom).toFixed(3)}`, canvas.clientWidth - (i+5), (canvas.clientHeight/2)+14);

            this.lineTo(canvas, i, (canvas.clientHeight/2)+5, i, (canvas.clientHeight/2)-5);
            this.lineTo(canvas, canvas.clientWidth - i, (canvas.clientHeight/2)+5, canvas.clientWidth - i, (canvas.clientHeight/2)-5);
            numb++;
        }
    }

    drawVerticalLine(canvas) {
        let ctx = canvas.getContext("2d");
        ctx.beginPath();
        ctx.moveTo( canvas.clientWidth/2 + 1,  0);
        ctx.lineTo( canvas.clientWidth/2 + 1, canvas.clientHeight);
        ctx.strokeStyle = '#000000';
        ctx.stroke();
    }

    drawHorizontalLine(canvas) {
        let ctx = canvas.getContext("2d");
        ctx.beginPath();
        ctx.moveTo(0,  canvas.clientHeight/2);
        ctx.lineTo(canvas.clientWidth, canvas.clientHeight/2);
        ctx.strokeStyle = '#000000';
        ctx.stroke();
    }

    lineTo(canvas, x1, y1, x2, y2, color = 'black') {
        var ctx = canvas.getContext("2d");
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.strokeStyle = color;
        ctx.stroke();
    }

    gotoxy(canvas, x1, y1, x2, y2) {
        x1 = (canvas.clientWidth / 2) + x1;
        y1 = (canvas.clientHeight / 2) - y1;
        x2 = (canvas.clientWidth / 2) + x2;
        y2 = (canvas.clientHeight / 2) - y2;
        
        var ctx = canvas.getContext("2d");
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.strokeStyle = '#ce2d2d';

        ctx.stroke();
    }

    text(canvas, text, x, y, size = 10) {
        let ctx = canvas.getContext('2d');
        ctx.fillStyle = "#000000";
        ctx.font = `normal ${size}px sans-serif`;
        ctx.fillText(text, x, y);
    }
}