class Tokenizator {

    constructor() {}

    tokenize(str) {
        str = str.replace(/\s/g, '');
        str = str.split("");
    
        let result = [];
        let buffer = {
            letter: [],
            number: []
        }
    
        str.forEach((char) => {
            if(this.isDigit(char)) {
                buffer.number.push(char);

            } else if(char === ".") {
                buffer.number.push(char);

            } else if (this.isLetter(char)) {
                if(buffer.number.length) {
                    result = this.emptyNumberBuffer(buffer, result);                    
                    result.push(new Token(Tokens.Operator, "*"));
                }
                buffer.letter.push(char);

            } else if (this.isOperator(char)) {
                result = this.emptyNumberBuffer(buffer, result);
                result = this.emptyLetterBuffer(buffer, result);

                result.push(new Token(Tokens.Operator, char));

            } else if (this.isLeftParenthesis(char)) {
                if(buffer.letter.length) {
                    result.push(new Token(Tokens.Function, buffer.letter.join("")));
                    buffer.letter = [];

                } else if(buffer.number.length) {
                    result = this.emptyNumberBuffer(buffer, result);

                    result.push(new Token(Tokens.Operator, "*"));

                }
                result.push(new Token(Tokens.LeftParenthesis, char));
            } else if (this.isRightParenthesis(char)) {

                result = this.emptyLetterBuffer(buffer, result);
                result = this.emptyNumberBuffer(buffer, result);

                result.push(new Token(Tokens.RightParenthesis, char));

            } else if (this.isComma(char)) {

                result = this.emptyNumberBuffer(buffer, result);
                result = this.emptyLetterBuffer(buffer, result);

                result.push(new Token(Tokens.FunctionArgument, char));
            } else {
                throw `Unknow symbol: ${char}`;
            }
        });
        
        if (buffer.number.length) {
            result = this.emptyNumberBuffer(buffer, result);
        }
        if(buffer.letter.length) {
            result = this.emptyLetterBuffer(buffer, result);
        }
        return result;
    }

    emptyNumberBuffer(buffer, result) {
        if(buffer.number.length) {
            result.push(new Token(Tokens.Literal, buffer.number.join("")));
            buffer.number = [];
        }

        return result;
    }

    emptyLetterBuffer(buffer, result) {
        if(buffer.letter.length) {
            result.push(new Token(Tokens.Variable, buffer.letter.join("")));
            buffer.letter = [];
        }

        return result;
    }



    isComma(ch) {
        return /,/.test(ch);
    }
    
    isDigit(ch) {
        return /\d/.test(ch);
    }
    
    isLetter(ch) {
        return /[a-z]/i.test(ch);
    }
    
    isOperator(ch) {
        return /\+|-|\*|\/|\^/.test(ch);
    }
    
    isLeftParenthesis(ch) {
        return /\(/.test(ch);
    }
    
    isRightParenthesis(ch) {
        return /\)/.test(ch);
    }
}