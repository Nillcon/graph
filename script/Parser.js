class Parser {
    constructor() { }

    parse(inp) {
        let outStack = [];
        let opStack = [];

        //tokenize
        let tokens = tokenizator.tokenize(inp);

        tokens.forEach(function(v) {
            if(v.type === Tokens.Literal) {
                outStack.push(new NumberASTNode(Number(v.value)));
            } else if (v.type === Tokens.Variable)  {
                switch (v.value) {
                    case 'e':
                        outStack.push(new EASTNode(v.value));
                        break;
                    case 'pi':
                        outStack.push(new PiASTNode(v.value));
                        break;
                    default:
                        outStack.push(new VariableASTNode(v.value));
                }
            } else if(v.type === Tokens.Function) {
                opStack.push(v);
            } else if(v.type === Tokens.FunctionArgument) {
                let stackParenthesis = [];
                while(opStack.peek() && opStack.peek().type !== Tokens.LeftParenthesis && !stackParenthesis.length) {
                    if (opStack.peek().type === Tokens.RightParenthesis) {
                        stackParenthesis.push();
                    } else {
                        stackParenthesis.pop();
                    }
                    outStack.addNode(opStack.pop());
                }
            } else if(v.type === Tokens.Operator) {
                while (opStack.peek() && (opStack.peek().type === Tokens.Operator)) {
                    outStack.addNode(opStack.pop());
                }
                opStack.push(v);
            } else if(v.type === Tokens.LeftParenthesis) {
                opStack.push(v);
            } else if(v.type === Tokens.RightParenthesis) {
                while(opStack.peek()  && opStack.peek().type !== Tokens.LeftParenthesis) {
                    outStack.addNode(opStack.pop());
                }
                opStack.pop();

                if(opStack.peek() && opStack.peek().type === Tokens.Function) {
                    outStack.addNode(opStack.pop());
                }
            }
        });

        while(opStack.peek()) {
            outStack.addNode(opStack.pop());
        }
    
        return outStack.pop();
    }

    getNode(operatorToken, outStack) {
        let op = operatorToken.value;
        let m1 = outStack.pop();
    
        switch (op) {
            case 'sin':
                return new SinASTNode(m1);
            case 'cos':
                return new CosASTNode(m1);
            case 'tan':
                return new TanASTNode(m1);
            case '+': 
                return new PlusASTNode(m1, outStack.pop());
            case '-': 
                if (outStack.length) {
                    return new MinusASTNode(outStack.pop(), m1);
                } else {
                    return new UnaryMinusASTNode(m1);
                }
            case '/': 
                return new DivideASTNode(outStack.pop(), m1);
            case '*': 
                return new MultiplyASTNode(outStack.pop(), m1);
            case '^': 
                return new ExponentiationASTNode(outStack.pop(), m1);
            default:
                throw 'Unknowed function';
        }
    }
}