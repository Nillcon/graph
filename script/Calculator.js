class Calculator {
    calcSquare(tree, i1, i2, step) {
        let sum = 0;

        for (let x = i1; x <= i2; x += step) {

            let res = step * tree.calculate({x: x});

            if (res && (res != Infinity) && (res != -Infinity)) {
                sum += res;
            }            
        }

        return this.round(sum);
    }

    round(num, dec = 4) {
        return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    }
}