const assoc = {
    '^' : 'right',
    '*' : 'left',
    '/' : 'left',
    '+' : 'left',
    '-' : 'left'
};

const prec = {
    '^' : 4,
    '*' : 3,
    '/' : 3,
    '+' : 2,
    '-' : 2
};

const Tokens = {
    Operator: 1,
    Function: 2,
    LeftParenthesis: 3,
    RightParenthesis: 4,
    FunctionArgument: 5,
    Literal: 6,
    Variable: 7
}

class Token {
    constructor(type, value) {
        this.type = type;
        this.value = value;
    }

    precedence() {
        return prec[this.value];
    }

    associativity() {
        return assoc[this.value];
    }
}