Array.prototype.peek = function() {
    return this.slice(-1)[0];
};

Array.prototype.addNode = function (token) {
    let node = parser.getNode(token, this);

    this.push(node);
}