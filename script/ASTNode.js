class ASTNode {
    constructor(token, variables) {
        this.op = token;
    }

    calculate(variables) {}
    derivative() {}
}

class UnaryASTNode extends ASTNode {
    constructor (token, m1) {
        super(token);
        this.m1 = m1;
    }

    calculate(variables) {}
    derivative() {}
}

class BinaryASTNode extends ASTNode {
    constructor (token, m1, m2) {
        super(token);

        this.m1 = m1;
        this.m2 = m2;
    }

    calculate(variables) {}

    derivative() {}
}

class VariableASTNode extends ASTNode {
    constructor (token) {
        super(token);
    }

    calculate(variables) {
        for (let v in variables) {
            if (this.op === v) {
                return variables[v];
            }
        }
        throw `Unknowed variable: ${this.op}`;
    }

    derivative(){
        return this;
    }
}

class NumberASTNode extends ASTNode {
    constructor (token) {
        super(token);
    }

    calculate(variables) {
        return this.op;
    }

    derivative(){
        return this;
    }
}

class SinASTNode extends UnaryASTNode {
    constructor (m1) {
        super('sin', m1);
    }

    calculate(variables) {
        let a = this.m1.calculate(variables);

        return calculator.round(Math.sin(a));
    }

    derivative() {
        return new CosASTNode(this.m1.derivative());
    }
}
class CosASTNode extends UnaryASTNode {
    constructor (m1) {
        super('cos', m1);
    }

    calculate(variables) {
        let a = this.m1.calculate(variables);

        return calculator.round(Math.cos(a));
    }


    derivative() {
        let d1 = new SinASTNode(this.m1.derivative());
        return new UnaryMinusASTNode(d1);
    }
}



class PiASTNode extends ASTNode {
    constructor () {
        super('PI');
    }

    calculate(variables) {
        return Math.PI;
    }

    derivative(){
        return this;
    }
}

class EASTNode extends ASTNode {
    constructor (token) {
        super(token);
    }

    calculate(variables) {
        return Math.E;
    }

    derivative(){
        return this;
    }
}



class TanASTNode extends UnaryASTNode {
    constructor (m1) {
        super('tan', m1);
    }

    calculate(variables) {
        let a = this.m1.calculate(variables);

        return calculator.round(Math.tan(a));
    }

    derivative() {
        let d1 = new NumberASTNode(1);
        let d2 = new CosASTNode(this.m1.derivative());

        let d3 = new ExponentiationASTNode(d2, new NumberASTNode(2));

        return new DivideASTNode(d1, d3);        
    }
}

class UnaryMinusASTNode extends UnaryASTNode {
    constructor (m1) {
        super('-', m1);
    }

    calculate(variables) {
        let a = this.m1.calculate(variables);

        return -a;
    }

    derivative() {
        return new UnaryMinusASTNode(this.m1.derivative());
    }
}

class PlusASTNode extends BinaryASTNode {
    constructor (m1, m2) {
        super('+', m1, m2);
    }

    calculate(variables) {
        let a = this.m1.calculate(variables);
        let b = this.m2.calculate(variables);

        return calculator.round(a + b);
    }

    derivative() {
        return new PlusASTNode(this.m1.derivative(), this.m2.derivative());
    }
}

class MinusASTNode extends BinaryASTNode {
    constructor (m1, m2) {
        super('-', m1, m2);
    }

    calculate(variables) {
        let a = this.m1.calculate(variables);
        let b = this.m2.calculate(variables);
        return calculator.round(a - b);
    }

    derivative() {
        return new MinusASTNode(this.m1.derivative(), this.m2.derivative());
    }
}

class DivideASTNode extends BinaryASTNode {
    constructor (m1, m2) {
        super('/', m1, m2);
        console.log(m1, m2);
    }

    calculate(variables) {
        let a = this.m1.calculate(variables);
        let b = this.m2.calculate(variables);

        return calculator.round(a / b);
    }

    derivative() {
        let uf = this.m1.derivative();
        let vf = this.m2.derivative();

        let b1 = new MultiplyASTNode(uf, this.m2);
        let b2 = new MultiplyASTNode(vf, this.m1);
        let b3 = new MinusASTNode(b1, b2);
        
        let b4 = new ExponentiationASTNode(this.m2, new NumberASTNode(2));

        return new DivideASTNode(b3, b4);
    }
}

class MultiplyASTNode extends BinaryASTNode {
    constructor (m1, m2) {
        super('*', m1, m2);
    }

    calculate(variables) {
        let a = this.m1.calculate(variables);
        let b = this.m2.calculate(variables);

        return calculator.round(a * b);
    }

    derivative() {
        let uf = this.m1.derivative();
        let vf = this.m2.derivative();

        let b1 = new MultiplyASTNode(uf, this.m2);
        let b2 = new MultiplyASTNode(this.m1, vf);

        return new PlusASTNode(b1, b2);
    }
}

class ExponentiationASTNode extends BinaryASTNode {
    constructor (m1, m2) {
        super('^', m1, m2);
    }

    calculate(variables) {
        let a = this.m1.calculate(variables);
        let b = this.m2.calculate(variables);
        return calculator.round(Math.pow(a, b));
    }

    derivative() {
        let b1 = new MinusASTNode(this.m2.derivative(), new NumberASTNode(1));
        let b2 = new ExponentiationASTNode(this.m1, b1);

        return new MultiplyASTNode(this.m2.derivative(), b2);
    }
}